<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransactionController;

Route::view('/', 'welcome');

Route::view('dashboard', 'dashboard')
    ->middleware(['auth', 'verified'])
    ->name('dashboard');

Route::view('transactions', 'transaction.index')
    ->middleware(['auth', 'verified'])
    ->name('transactions.index');

Route::view('transactions/create', 'transaction.create')
    ->middleware(['auth', 'verified'])
    ->name('transactions.create');

Route::view('profile', 'profile')
    ->middleware(['auth'])
    ->name('profile');

require __DIR__.'/auth.php';
