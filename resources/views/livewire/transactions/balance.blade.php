<div class="w-[400px]">
  <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-xl">
    <div class="p-6 text-gray-900 dark:text-gray-100 flex flex-col gap-2">
      <span class="text-slate-400 text-center">{{ __("Balance") }}</span>
      <span class="font-light text-4xl">Rp. {{ number_format($balance, 2, ',', '.') }}</span>
    </div>
  </div>
</div>