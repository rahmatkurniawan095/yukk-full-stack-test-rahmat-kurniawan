<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;

use function Livewire\Volt\rules;
use function Livewire\Volt\{state, usesFileUploads};

usesFileUploads();

state([
    'amount' => 0,
    'description' => '',
    'type' => '',
    'attachment' => ''
]);

rules([
    'amount' => ['required', 'numeric', 'min:1000'],
    'description' => ['required', 'string'],
    'type' => ['required', 'string'],
    'attachment' => ['required_if:type,topup']
]);

$create = function () {
    try {
      $validated = $this->validate();
    } catch (ValidationException $e) {
      throw $e;
    }
    
    $user_id = Auth::user()->id;
    $count = DB::table('transactions')->count() + 1;

    $code_trx = "$user_id-$count";
    if ($validated["type"] == "topup") {
      $code_trx = "TOP-" . $code_trx;
    } else {
      $code_trx = "TRX-" . $code_trx;
    }

    $filename = "";
    if ($validated['type'] == 'topup') {
      $file = $this->attachment->store('public/attachment/');
      $split = explode("//", $file);
      $filename = $split[1] ?? "";
    }

    DB::table('transactions')->insert([
      'code' => $code_trx,
      'user_id' => $user_id,
      'type' => $validated['type'],
      'amount' => $validated['amount'],
      'description' => $validated['description'],
      'attachment' => $filename,
      'created_at' => now()
    ]);

    $this->redirect('/transactions', navigate: true);
};

?>

<section>
    <header>
        <h2 class="text-lg font-medium text-gray-900 dark:text-gray-100">
            {{ __('Create') }}
        </h2>

        <p class="mt-1 text-sm text-gray-600 dark:text-gray-400">
            {{ __('Create new Transaction or Top Up.') }}
        </p>
    </header>

    <form wire:submit="create" class="mt-6 space-y-6">
        <div>
          <x-input-label for="type" :value="__('Type')" />
          <select name="type" id="type" wire:model="type" class="border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm mt-1 block w-full">
            <option value="">Select Type</option>
            <option value="topup">Top Up</option>
            <option value="transaction">Transaksi</option>
          </select>
          <x-input-error :messages="$errors->get('type')" class="mt-2" />
        </div>

        <div>
            <x-input-label for="amount" :value="__('Amount')" />
            <x-text-input wire:model="amount" id="amount" name="amount" type="text" class="mt-1 block w-full" />
            <x-input-error :messages="$errors->get('amount')" class="mt-2" />
        </div>

        <div>
            <x-input-label for="description" :value="__('Description')" />
            <textarea wire:model="description" id="description" name="description" class="border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm mt-1 block w-full"></textarea>
            <x-input-error :messages="$errors->get('description')" class="mt-2" />
        </div>

        <div x-show="$wire.type == 'topup'">
          <input type="file" wire:model="attachment">
          <x-input-error :messages="$errors->get('attachment')" class="mt-2" />
        </div>

        <div class="flex items-center gap-4">
            <x-primary-button>{{ __('Create') }}</x-primary-button>

            <x-action-message class="me-3" on="password-updated">
                {{ __('Created.') }}
            </x-action-message>
        </div>
    </form>
</section>
