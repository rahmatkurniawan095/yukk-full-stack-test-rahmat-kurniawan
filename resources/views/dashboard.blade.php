<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 flex flex-col gap-5">
            <div class="bg-white dark:bg-cyan-500/10 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-cyan-500">
                    {{ __("You're logged in!") }}
                </div>
            </div>

            <livewire:transactions.balance>
        </div>
    </div>
</x-app-layout>
