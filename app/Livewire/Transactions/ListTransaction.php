<?php

namespace App\Livewire\Transactions;
 
use App\Models\Transaction;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Contracts\HasTable;
use Filament\Tables\Table;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Filament\Tables\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;
use Filament\Tables\Filters\TernaryFilter;
use Illuminate\Support\Facades\Auth;

class ListTransaction extends Component implements HasForms, HasTable
{
    use InteractsWithTable;
    use InteractsWithForms;

    public function table(Table $table): Table
    {
        return $table
            ->query(Transaction::query()->where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC'))
            ->columns([
                TextColumn::make('code')->searchable(),
                TextColumn::make('type')->badge()->color(fn (string $state): string => match ($state) {
                    'topup' => 'success',
                    'transaction' => 'danger',
                }),
                TextColumn::make('amount')->numeric(locale: 'id', decimalPlaces: 2),
                TextColumn::make('description')->searchable(),
            ])
            ->filters([
                TernaryFilter::make('type Transaction')
                    ->label('Type')
                    ->trueLabel('Top Up')
                    ->falseLabel('Transaction')
                    ->queries(
                        true: fn (Builder $query) => $query->where('type', 'topup'),
                        false: fn (Builder $query) => $query->where('type', 'transaction'),
                        blank: fn (Builder $query) => 0, 
                    )
            ]);
    }

    public function render()
    {
        return view('livewire.transactions.list-transaction');
    }
}