<?php

namespace App\Livewire\Transactions;
 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class Balance extends Component
{
    public function render()
    {
      $user_id = Auth::user()->id;
      $total_topup = DB::table('transactions')->where("user_id", $user_id)->where("type", "topup")->sum("amount");
      $total_transactions = DB::table('transactions')->where("user_id", $user_id)->where("type", "transaction")->sum("amount");
      $balance = $total_topup - $total_transactions;

      return view('livewire.transactions.balance', ["balance" => $balance]);
    }
}